package gun.gui;

import gun.model.ArtilleryModel;
import gun.model.ArtilleryModelListener;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class GunCanvas extends JPanel implements 
  ArtilleryModelListener
{
  
  // assets
  private Image tankImage;
  private Image gunImage;
  private ArtilleryModel model = null;
  
  // path recording
  ArrayList<ShellPath> paths = new ArrayList<ShellPath>();
  ShellPath currentPath = null;
  
  // for sake of concurrency
  ReentrantLock lock = new ReentrantLock();
  
  public GunCanvas(Image aTankImage, Image aGunImage){
    this.setBackground(Color.WHITE);
    this.tankImage = aTankImage;
    this.gunImage = aGunImage;
  }
  
  public void paint(Graphics g){
    lock.lock();
    super.paint(g);
    lock.unlock();
  }

  
  public void paintComponent(Graphics g1){
    super.paintComponent(g1);
    if(model != null){
      Graphics2D g = (Graphics2D) g1;
      
      RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, 
          RenderingHints.VALUE_ANTIALIAS_ON);
      g.setRenderingHints(rh);
      
      // draw tank image
      drawImageInPlace(g, tankImage, model.getTankPosition());
      
      // draw gun image
      drawImageInPlace(g, gunImage, model.getGunPosition());
    
      // draw angle
      g.setColor(Color.black);
      g.setStroke(new BasicStroke(2));
      Point2D.Double gun = model.getGunPosition();
      double len = 2 * Math.max(gunImage.getWidth(null), gunImage.getHeight(null));
      double x = len * Math.cos(model.getGunAngle());
      double y = len * Math.sin(model.getGunAngle());
      Point2D.Double end = new Point2D.Double(x + gun.getX(), y + gun.getY());
      
      g.draw(new Line2D.Double(gun, end));
      g.draw(new Line2D.Double(gun, new Point2D.Double(len + gun.getX(),gun.getY())));
      g.setColor(Color.red);
      
      g.draw(new Arc2D.Double(
          gun.getX() - len / 2, gun.getY() - len / 2,
          len, len,
          0,
          -Math.toDegrees(model.getGunAngle()),
          Arc2D.OPEN));
      
      // draw shell, if it's in motion
      
      model.lock();
      if (currentPath != null) {
        currentPath.draw(g);
      }
      model.unlock();
 
      
      // draw all paths
      for(ShellPath p : paths)
        p.draw(g);
    }
  }
  
  
  private void drawImageInPlace(Graphics2D g, Image img, Point2D.Double place){
    int w = img.getWidth(null) / 2;
    int h = img.getHeight(null) / 2;
    
    g.drawImage(img, (int)place.getX() - w, (int)place.getY() - h, null);
  }
 

  @Override
  public void settingsChanged(ArtilleryModel m) {
    repaint();
  }

  
  @Override
  public void shellMoved(ArtilleryModel m) {
    repaint();
    if(currentPath != null)
      currentPath.add(m.getShellPosition());
  }
  
  
  public void setModel(ArtilleryModel model){
    this.model = model;
  }
  
  
  public Color randomColor(){
    Random rand = new Random();
    float r = rand.nextFloat();
    float g = rand.nextFloat();
    float b = rand.nextFloat();
    
    return new Color(r,g,b);
  }
  
  
  public void beginPathRecording(){
    currentPath = new ShellPath(randomColor());
  }
  
  
  public void endPathRecording(){
    paths.add(currentPath);
    currentPath = null;
  }
  
  public void clear(){
    paths.clear();
    repaint();
  }
  
  public double getTankSize(){
    return Math.min(tankImage.getWidth(null), tankImage.getHeight(null));
  }
}
