package gun.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.LinkedList;

public class ShellPath {
  private LinkedList<Point2D> points = new LinkedList<Point2D>();
  private Color color;

  public ShellPath(Color color){
    this.color = color;
  }
  
  public void draw(Graphics2D g){
    g.setColor(color);
    g.setStroke(new BasicStroke(1));
    if(points.size() != 0){
      Point2D prev = points.get(0);
      for(Point2D cur : points){
        g.draw(new Line2D.Double(prev, cur));
        prev = cur;
      }
    }
  }
  
  public void add(Point2D x){
    points.add(x);
  }
}
