package gun.gui;

import gun.controller.ArtilleryController;
import gun.model.ArtilleryModel;
import gun.model.ArtilleryModelListener;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;

import net.miginfocom.swing.MigLayout;



@SuppressWarnings("serial")
public class Main extends JFrame implements ArtilleryModelListener{
  // ui components
  private JPanel tools = new JPanel();
  private JButton gunButton = new JButton();
  private JButton tankButton =  new JButton();
  private JSpinner angleSpinner = new JSpinner();
  private JSpinner velocitySpinner = new JSpinner();
  private JSpinner dragSpinner = new JSpinner();
  private JButton tryButton = new JButton("Try");
  private JButton aimButton = new JButton("Aim for me");
  private JButton clearButton = new JButton("Clear view");
  private GunCanvas canvas;
  
  // spinner models
  private SpinnerNumberModel angleModel = new SpinnerNumberModel(
      ArtilleryModel.defaultAngle,
      ArtilleryModel.minAngle,
      ArtilleryModel.maxAngle,
      0.05);
  
  private SpinnerNumberModel velocityModel = new SpinnerNumberModel(
      ArtilleryModel.defaultVelocity,
      ArtilleryModel.minVelocity,
      ArtilleryModel.maxVelocity,
      1.0);
  
  private SpinnerNumberModel dragModel = new SpinnerNumberModel(
      ArtilleryModel.defaultDrag,
      ArtilleryModel.minDrag,
      ArtilleryModel.maxDrag,
      1.0);
  
  
  // assets
  private ImageIcon tankIcon;
  private ImageIcon gunIcon;
  
  // model
  private ArtilleryModel model = new ArtilleryModel();
  
  // controller
  private ArtilleryController controller ;
  
  public static void main(String[] args){
    @SuppressWarnings("unused")
    Main m = new Main();
  }


  public Main(){
    setupUI();
    
    canvas.setModel(model);
    controller.setModel(model);
    connectEvents();
    setVisible(true);
  }
  
  private void connectEvents(){
    canvas.addMouseListener(new MouseAdapter(){
      public void mouseClicked(MouseEvent e){
        controller.canvasClicked(e);
      }
    });
    
    model.addListener(canvas);
    
    tryButton.addActionListener(new ActionAdapter(){
      public void actionPerformed(ActionEvent e){
        controller.tryClicked();
      }
    });
  
    tankButton.addActionListener(new ActionAdapter(){
      public void actionPerformed(ActionEvent e){
        controller.tankClicked();
      }
    });
    
    gunButton.addActionListener(new ActionAdapter(){
      public void actionPerformed(ActionEvent e){
        controller.gunClicked();
      }
    });
    
    angleSpinner.addChangeListener(new ChangeAdapter(){
      public void stateChanged(ChangeEvent e){
        controller.angleChanged(angleModel.getNumber().doubleValue());
      }
    });
    
    velocitySpinner.addChangeListener(new ChangeAdapter(){
      public void stateChanged(ChangeEvent e){
        controller.velocityChanged(velocityModel.getNumber().doubleValue());
      }
    });
    
    dragSpinner.addChangeListener(new ChangeAdapter(){
      public void stateChanged(ChangeEvent e){
        controller.dragChanged(dragModel.getNumber().doubleValue());
      }
    });
    
    clearButton.addActionListener(new ActionAdapter(){
      public void actionPerformed(ActionEvent e){
        canvas.clear();
      }
    });
    
    aimButton.addActionListener(new ActionAdapter(){
      public void actionPerformed(ActionEvent e){
        controller.aimClicked();
      }
    });
    
    controller.getModel().addListener(this);
  }
  
  private void setupUI(){
    // try to set nice look and feel
    try {
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
    } catch (Exception e) {
      e.printStackTrace();
    }
    // load assets
    tankIcon = new ImageIcon(getClass().getResource("/tank.png"));
    gunIcon = new ImageIcon(getClass().getResource("/flak.png"));
    
    canvas = new GunCanvas(tankIcon.getImage(), gunIcon.getImage());
    controller = new ArtilleryController(canvas);
    
    setTitle("Gun");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setPreferredSize(new Dimension(800,600));
    getContentPane().setLayout(new BorderLayout());
    tools.setLayout(new MigLayout("wrap 2"));
    
    tools.add(new JSeparator(JSeparator.HORIZONTAL), "growx, span");
    tools.add(new JLabel("Objects"), "growx, span");
    tankButton.setIcon(tankIcon);
    gunButton.setIcon(gunIcon);
    tools.add(tankButton, "growx, span");
    tools.add(gunButton, "growx, span");
    tools.add(new JSeparator(JSeparator.HORIZONTAL), "growx, span");
    
    tools.add(new JLabel("Control"), "growx, span");
    
    tools.add(new JLabel("α = "));
    tools.add(angleSpinner, "growx");
    
    tools.add(new JLabel("v = "));
    tools.add(velocitySpinner, "growx");
    
    tools.add(new JLabel("c/m = "));
    tools.add(dragSpinner, "growx");
    
    tools.add(tryButton, "growx, span");
    tools.add(aimButton, "growx, span");
    
    tools.add(new JSeparator(JSeparator.HORIZONTAL), "growx, span");
    tools.add(new JLabel("View"), "growx, span");
    tools.add(clearButton, "growx, span");
    
    getContentPane().add(canvas, BorderLayout.CENTER);
    
    getContentPane().add(tools, BorderLayout.LINE_START);
    
    angleSpinner.setModel(angleModel);
    velocitySpinner.setModel(velocityModel);
    dragSpinner.setModel(dragModel);
    
    pack();
  }


  @Override
  public void settingsChanged(ArtilleryModel m) {
    angleModel.setValue(model.getGunAngle());
    dragModel.setValue(m.getDrag());
    velocityModel.setValue(m.getVelocity());
  }


  @Override
  public void shellMoved(ArtilleryModel m) {}
}

