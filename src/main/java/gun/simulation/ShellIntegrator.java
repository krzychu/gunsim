package gun.simulation;

import gun.model.ArtilleryModel;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

public class ShellIntegrator {
  private ArtilleryModel model;
  private ShellState state;
  
  public ShellIntegrator(ArtilleryModel model, double alpha){
    this.model = model;
    
    // extract shell parameters from model
    state = new ShellState();
    state.x = model.getGunPosition().x;
    state.y = model.getGunPosition().y;
    state.vx = model.getVelocity() * Math.cos(alpha);
    state.vy = model.getVelocity() * Math.sin(alpha);
     
  }
  
  public void advance(double dt){
    // rk4
    ShellState cur = state.clone();
    ShellState k1 = derivative(cur);
    
    cur.accumulate(1.0/2.0 * dt, k1);
    ShellState k2 = derivative(cur);
    
    cur = state.clone();
    cur.accumulate(1.0/2.0 * dt, k2);
    ShellState k3 = derivative(cur);
  
    cur = state.clone();
    cur.accumulate(dt, k3);
    ShellState k4 = derivative(cur);
    
    state.accumulate(1.0/6.0 * dt,  k1); 
    state.accumulate(1.0/3.0 * dt,  k2);
    state.accumulate(1.0/3.0 * dt,  k3);
    state.accumulate(1.0/6.0 * dt,  k4);
  }
  
  private ShellState derivative(ShellState s){
    ShellState d = new ShellState();
    double vx = s.vx;
    double vy = s.vy;
    d.x = s.vx;
    d.vx = -model.getDrag() * vx * Math.sqrt(vx*vx + vy*vy) / 10000.0;
    d.y = s.vy;
    d.vy = -model.getG() - model.getDrag() * vy * Math.sqrt(vx*vx + vy*vy)/ 10000.0;
    return d;
  }
  
  public Point2D.Double getShellPosition(){
    return new Point2D.Double(state.x, state.y);
  }
}
