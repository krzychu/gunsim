package gun.simulation;

public class ShellState{
  public double x = 0;
  public double vx = 0;
  public double y = 0;
  public double vy = 0;
  
  public void accumulate(double c, ShellState other){
    x += c * other.x;
    y += c * other.y;
    vx += c * other.vx;
    vy += c * other.vy;
  }
  
  public ShellState clone(){
    ShellState n = new ShellState();
    n.x = x;
    n.y = y;
    n.vx = vx;
    n.vy = vy;
    return n;
  }
}
