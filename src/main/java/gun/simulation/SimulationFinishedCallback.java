package gun.simulation;

public interface SimulationFinishedCallback {
  public void simulationFinished();
}
