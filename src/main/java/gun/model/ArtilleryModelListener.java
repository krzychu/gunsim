package gun.model;

public interface ArtilleryModelListener {
  public void settingsChanged(ArtilleryModel m);
  public void shellMoved(ArtilleryModel m);
}
