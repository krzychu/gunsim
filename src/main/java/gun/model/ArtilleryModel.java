package gun.model;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

public class ArtilleryModel {
  // Observer
  private LinkedList<ArtilleryModelListener> listeners = new
      LinkedList<ArtilleryModelListener>();
  
  private Point2D.Double gunPosition = new Point2D.Double(-100,-100);
  private Point2D.Double tankPosition = new Point2D.Double(-200,-100);
  private Point2D.Double shellPosition = new Point2D.Double(-100, -100);
 
  
  private double gunAngle = 0;
  private double velocity = 0;
  private double drag;
  
  public static final double defaultAngle = -Math.PI / 4;
  public static final double minAngle = -Math.PI;
  public static final double maxAngle = Math.PI;
  
  public static final double defaultVelocity = 1000.0;
  public static final double minVelocity = 1.0;
  public static final double maxVelocity = 100000.0;
  
  public static final double defaultDrag = 40.0;
  public static final double minDrag = 0.0;
  public static final double maxDrag = 10000.0;
  
  private ReentrantLock lock = new ReentrantLock();
  
  public ArtilleryModel(){
    gunAngle = defaultAngle;
    velocity = defaultVelocity;
    drag = defaultDrag;
  }
  
  public void lock(){
    lock.lock();
  }
  
  public void unlock(){
    lock.unlock();
  }
  
  public void addListener(ArtilleryModelListener listener){
    listeners.add(listener);
    listener.settingsChanged(this);
  }
  
  private void fireSettingsCallbacks(){
    for(ArtilleryModelListener listener : listeners){
      listener.settingsChanged(this);
    }
  }
  
  private void fireShellsCallbacks(){
    for(ArtilleryModelListener listener : listeners){
      listener.shellMoved(this);
    }
  }

  public Point2D.Double getGunPosition() {
    return gunPosition;
  }

  public void setGunPosition(Point2D.Double gunPosition) {
    this.gunPosition = gunPosition;
    fireSettingsCallbacks();
  }

  public Point2D.Double getTankPosition() {
    return tankPosition;
  }

  public void setTankPosition(Point2D.Double tankPosition) {
    this.tankPosition = tankPosition;
    fireSettingsCallbacks();
  }
  
  public Point2D.Double getShellPosition() {
    return shellPosition;
  }

  public void setShellPosition(Point2D.Double shellPosition) {
    this.shellPosition = shellPosition;
    fireShellsCallbacks();
  }

  public double getGunAngle() {
    return gunAngle;
  }

  public void setGunAngle(double gunAngle) {
    this.gunAngle = gunAngle;
    fireSettingsCallbacks();
  }

  public double getVelocity() {
    return velocity;
  }

  public void setVelocity(double velocity) {
    this.velocity = velocity;
    fireSettingsCallbacks();
  }

  public double getDrag() {
    return drag;
  }

  public void setDrag(double drag) {
    this.drag = drag;
    fireSettingsCallbacks();
  }
  
  public double getG(){
    return -10;
  }
}
