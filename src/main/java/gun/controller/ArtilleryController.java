package gun.controller;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;

import gun.gui.GunCanvas;
import gun.model.ArtilleryModel;
import gun.simulation.AimBot;
import gun.simulation.ShellIntegrator;
import gun.simulation.SimThread;
import gun.simulation.SimulationFinishedCallback;


public class ArtilleryController{
  private ArtilleryModel model = null;
  private GunCanvas canvas;
  private AimBot bot;
  
  private enum State{PLACING_TANK, PLACING_GUN, IDLE, SIMULATING};
  private State state;
  
  public ArtilleryController(GunCanvas canvas){
    state = State.IDLE;
    this.canvas = canvas;
  }
  
  
  public void setModel(ArtilleryModel model) {
    this.model = model;
    bot = new AimBot(model);
  }

  public void canvasClicked(MouseEvent e) {
    int x = e.getPoint().x;
    int y = e.getPoint().y;
    
    if(state == State.PLACING_TANK){
      model.setTankPosition(new Point2D.Double(x,y));
    }
    else if(state == State.PLACING_GUN){
      model.setGunPosition(new Point2D.Double(x,y));
    }
    
    state = State.IDLE;
  }
  
  public void tryClicked(){
    if(state == State.SIMULATING)
      return;
    
    canvas.beginPathRecording();
    SimThread st = new SimThread(model, canvas.getTankSize()  /2.0, canvas.getWidth(), canvas.getHeight());
    state = State.SIMULATING;
    st.start(new SimulationFinishedCallback(){
      public void simulationFinished(){
        canvas.endPathRecording();
        state = State.IDLE;
      }
    });
    
    
  }
  
  public void gunClicked(){
    if(state == State.SIMULATING)
      return;
    state = State.PLACING_GUN;
  }
  
  public void tankClicked(){
    if(state == State.SIMULATING)
      return;
    state = State.PLACING_TANK;
  }
  
  public void aimClicked(){
    bot.aim();
  }
  
  public void angleChanged(double newAngle){
    model.setGunAngle(newAngle);
  }
  
  public void velocityChanged(double newVelocity){
    model.setVelocity(newVelocity);
  }
  
  public void dragChanged(double newDrag){
    model.setDrag(newDrag);
  }
  
  public ArtilleryModel getModel(){
    return model;
  }
}
